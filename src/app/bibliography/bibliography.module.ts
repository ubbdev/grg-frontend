import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { BibliographyRoutingModule } from './bibliography-routing.module';

import { BibliographyComponent } from './bibliography.component';
import { ReferenceIconPipe } from './reference-icon.pipe';
import { ReferenceContainerComponent } from './reference-container.component'

@NgModule({
  imports: [
    SharedModule,
    BibliographyRoutingModule
  ],
  declarations: [
    BibliographyComponent,
    ReferenceContainerComponent,
    ReferenceIconPipe
  ],
  exports: [
    ReferenceContainerComponent,
    ReferenceIconPipe
  ],
  providers: [  ]
})
export class BibliographyModule {}
