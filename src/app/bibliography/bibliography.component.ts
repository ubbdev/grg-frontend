import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { LoopBackAuth } from '../shared/sdk/services';
import { Reference, Project, Article, ReferenceCollection } from '../shared/sdk/models';
import { ReferenceApi, ProjectApi } from '../shared/sdk/services';
import { PROJECT_ID, ZOTERO_GROUPID, ZOTERO_GROUPNAME } from '../shared/project.config';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ReferenceSource } from '../shared/search-sources';

@Component({
  selector: 'app-bibliography',
  templateUrl: './bibliography.component.html',
  styleUrls: ['./bibliography.component.css']
})
export class BibliographyComponent implements OnInit {

  private title = 'Bibliography';
  private groupid = ZOTERO_GROUPID;
  private groupname = ZOTERO_GROUPNAME;
  private referenceCollections: Array<ReferenceCollection> = null;

  constructor(
    private referenceApi: ReferenceApi,
    private projectApi: ProjectApi,
    private router: Router,
    private auth: LoopBackAuth,
    private referenceSource: ReferenceSource,
    private titleService: Title) {
  }

  ngOnInit() {
    this.referenceSource.init();
    this.titleService.setTitle(this.title);

    this.projectApi
    .getReferenceCollectionTrees(PROJECT_ID)
    .subscribe(res => {
      this.referenceCollections = res;
    }, err => { console.log(err); });
  }

  collectionCount(collection: ReferenceCollection): Observable<number> {
    return this.referenceSource.aggregations$
    .map(aggs => {
      if (!(aggs && aggs.collections && aggs.collections.buckets)) {
        return 0;
      }

      const key = collection.key;
      let count = 0;

      const bucket = aggs.collections.buckets.find(collection => collection.key === key);

      if (bucket) {
        count += bucket.doc_count;
      }

      return count;
    });
  }
}
