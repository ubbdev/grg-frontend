import { Component, Input, OnInit } from '@angular/core';
import { Reference, Person } from '../shared/sdk/models';
import { LoopBackAuth, ProjectApi } from '../shared/sdk/services';
import { CurrentPersonService } from '../shared/sign-in/current-person.service';
import { PROJECT_ID } from '../shared/project.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reference-container',
  templateUrl: 'reference-container.component.html'
})
export class ReferenceContainerComponent implements OnInit {
  @Input() public reference: Reference;

  private isMentioned = false;
  private showReference = true;

  constructor(
    private auth: LoopBackAuth,
    private currentPerson: CurrentPersonService,
    private projectApi: ProjectApi,
    private router: Router) { }

  ngOnInit() {
    if (this.reference.articles && this.reference.articles.length > 0) this.isMentioned = true;
  }
}
