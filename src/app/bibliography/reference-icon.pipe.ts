import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'referenceIcon'
})
export class ReferenceIconPipe implements PipeTransform {
  transform(itemType: string, options: any): string {
    if (!options.size) options.size = 'huge';
    if (!options.largeIconColor) options.largeIconColor = 'grey';
    if (!options.smallIconColor) options.smallIconColor = 'green';

    let typeIcon = '';
    switch(itemType) {
      case 'Thesis':
      typeIcon = 'file outline text';`<i class="ui ${options.size} ${options.largeIconColor} file outline text icon"></i>`;
      break;

      case 'Journal article':
      typeIcon = 'file text';`<i class="ui ${options.size} ${options.largIconColor} file text icon"></i>`;
      break;

      case 'Chapter':
      typeIcon = 'file text';`<i class="${options.size} ${options.largIconColor} file text icon" ></i>`;
      break;

      case 'Book':
      default:
      typeIcon = 'book'; `<i class="ui ${options.size} ${options.largIconColor} book icon"></i>`;
    }
    if (options.assessed) {
      return `
      <i class="ui ${options.size} icons">
        <i class="${options.largeIconColor} ${typeIcon} icon"></i>
        <i class="${options.smallIconColor} corner check circle icon"></i>
      </i>`;
    }
    return `<i class="${options.size} ${options.largeIconColor} ${typeIcon} icon"></i>`

  }
}
