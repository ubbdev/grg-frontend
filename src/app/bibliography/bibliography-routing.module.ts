import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BibliographyComponent } from './bibliography.component';

import { AuthGuard } from '../shared/auth-guard.service';

const bibliographyRoutes: Routes = [
  {
    path: 'bibliography',
    component: BibliographyComponent
  },
];
@NgModule({
  imports: [ RouterModule.forChild(bibliographyRoutes) ],
  exports: [ RouterModule ]
})
export class BibliographyRoutingModule {}
