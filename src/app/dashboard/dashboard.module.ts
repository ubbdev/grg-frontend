import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardComponent } from './dashboard.component';
import { ActivityComponent } from './activity/activity.component';
import { UsersComponent } from './users/users.component';
import { AdministrationComponent } from './administration/administration.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [
    DashboardComponent,
    ActivityComponent,
    UsersComponent,
    AdministrationComponent,
  ]
})
export class DashboardModule { }
