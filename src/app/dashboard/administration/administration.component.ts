import { Component, OnInit } from '@angular/core';
import { PROJECT_ID } from '../../shared/project.config';
import { ReferenceApi, WorkApi, ProjectApi } from '../../shared/sdk/services';
import { Person, Article } from '../../shared/sdk/models';
import { AuthGuard } from '../../shared/auth-guard.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {
  private bibSyncStatus: any = {active: false, total: 1, progress: 0};

  constructor(
    private projectApi: ProjectApi,
    private referenceApi: ReferenceApi,
    private workApi: WorkApi,
    public authGuard: AuthGuard) {
  }

  ngOnInit() {
    this.subscribeToBibSyncStatus();
  }

  subscribeToBibSyncStatus() {
    const source = Observable.interval(1000);
    const statusSource = source.flatMap(() => this.referenceApi.syncStatus(PROJECT_ID));

    statusSource
    .map(status => { this.bibSyncStatus = status; return status; })
    .takeWhile(status => status.active === true)
    .subscribe(() => {});
  }

  syncBibliography() {
    this.referenceApi.sync(PROJECT_ID).toPromise().catch(error => console.log);

    this.subscribeToBibSyncStatus();
  }

  syncWorks() {
    this.workApi.sync(PROJECT_ID).toPromise().catch(error => console.log);
  }

  reindex() {
    this.projectApi.reindex(PROJECT_ID).toPromise().catch(error => console.log);
  }

}
