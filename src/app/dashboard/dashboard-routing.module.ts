import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ActivityComponent } from './activity/activity.component';
import { UsersComponent } from './users/users.component';
import { AdministrationComponent } from './administration/administration.component';

import { AuthGuard } from '../shared/auth-guard.service';

const dashboardRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '', redirectTo: 'activity', pathMatch: 'full'
      },
      {
        path: 'activity',
        component: ActivityComponent
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path:  'administration',
        component: AdministrationComponent
      }
    ]
  },
];
@NgModule({
  imports: [ RouterModule.forChild(dashboardRoutes) ],
  exports: [ RouterModule ]
})
export class DashboardRoutingModule {}
