import { Component, OnInit } from '@angular/core';
import { LoopBackAuth } from '../../shared/sdk/services';
import { PROJECT_ID } from '../../shared/project.config';
import { PersonApi, ReferenceApi, ProjectApi } from '../../shared/sdk/services';
import { Person, Article } from '../../shared/sdk/models';
import { CurrentPersonService } from '../../shared/sign-in/current-person.service';
import { AuthGuard } from '../../shared/auth-guard.service';
import { Observable } from 'rxjs/Observable';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../shared/messages/confirm-modal.component';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public persons: any[];
  project: any;
  public allRoles = ['admin', 'contributor', 'editor'];

  constructor(
    private personApi: PersonApi,
    private projectApi: ProjectApi,
    private auth: LoopBackAuth,
    private modalService: SuiModalService,
    public currentPerson: CurrentPersonService,
    public authGuard: AuthGuard) {
  }

  ngOnInit() {
    this.personApi.find()
    .flatMap(res => <any[]>res) // split array to separate observables
    .flatMap(person => { // decorate with count and roles
      person.roles = [];
      return Observable.forkJoin(
        this.personApi.getRoles(person.id),
        this.personApi.countArticles(person.id),
        (rolesResponse, countResponse) => {
          rolesResponse.forEach(role => { if (role.projectId === PROJECT_ID) person.roles.push(role.role); });
          person.articleCount = countResponse.count;
          return person;
        });
    })
    .toArray() // merge back to single array
    .subscribe(data => this.persons = data);
  }

  private personCanAddRole(role: string): Observable<boolean> {
    switch (role) {
      case 'contributor':
      return this.currentPerson.isProjectEditor();
      case 'editor':
      return this.currentPerson.isProjectEditor();
      case 'admin':
      return this.currentPerson.isProjectAdmin();
      default:
      return Observable.of(false);
    }
  }

  private personCanDeleteRole(personId: string, role: string): Observable<boolean> {
    switch (role) {
      case 'contributor':
      return this.currentPerson.isProjectEditor();
      case 'editor':
      return this.currentPerson.isProjectEditor();
      case 'admin':
      return Observable.zip(
        Observable.of(this.auth.getCurrentUserId() === personId),
        this.currentPerson.isProjectAdmin(),
        (isMe, imAdmin) => { return isMe && imAdmin; }
        );
      default:
      return Observable.of(false);
    }
  }

  private addRoleImpl(id, role) {

  }

  private addRole(id, role): void {
    const person = this.persons.find(obj => obj.id === id);
    this.personCanAddRole(role)
    .subscribe((canAdd: boolean) => {
      if (canAdd) {
        this.modalService
        .open(new ConfirmModal('Are you sure?', 'You are about to assign role \'' + role + '\' to ' +  person.name + '\''))
        .onApprove(() => {
          (() => {
            switch (role) {
            case 'contributor':
              return this.projectApi.addContributor(PROJECT_ID, id);
            case 'editor':
              return this.projectApi.addEditor(PROJECT_ID, id);
            case 'admin':
              return this.projectApi.addAdmin(PROJECT_ID, id);
            }
          })()
          .subscribe(res => {
            person.roles.push(role);
          });
        });
      }
    });
  }

  private deleteRole(id, role): void {
    const person = this.persons.find(obj => obj.id === id);
    this.personCanDeleteRole(id, role)
    .subscribe((canDelete: boolean) => {
      if (canDelete === true) {
        this.modalService
        .open(new ConfirmModal('Are you sure?', 'You are about to unassign role \'' + role + '\' from ' + person.name + '\''))
        .onApprove(() => {
          (() => {
            switch (role) {
            case 'contributor':
              return this.projectApi.removeContributor(PROJECT_ID, id);
            case 'editor':
              return this.projectApi.removeEditor(PROJECT_ID, id);
            case 'admin':
              return this.projectApi.removeAdmin(PROJECT_ID, id);
            }
          })()
          .subscribe(res => {
            // don't update display array until we get a response
            const roleIndex = person.roles.indexOf(role);
            if (roleIndex !== -1) {
              person.roles.splice(roleIndex, 1);
            }
          });
        });
      }
      // template should make sure we can only click when we can delete
    });
  }
}
