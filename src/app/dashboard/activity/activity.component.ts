import { Component, OnInit } from '@angular/core';
import { LoopBackAuth } from '../../shared/sdk/services';
import { PersonApi, ProjectApi, ArticleApi, EditApi } from '../../shared/sdk/services';
import { Person, Article, Edit } from '../../shared/sdk/models';
import { PROJECT_ID } from '../../shared/project.config';
import { CurrentPersonService } from '../../shared/sign-in/current-person.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ErrorModal } from '../../shared/messages/error-modal.component';
import { ConfirmModal } from '../../shared/messages/confirm-modal.component';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {

  private person: Person = new Person;
  private contributions: Article[] = [];
  private drafts: Article[] = [];
  private publicDrafts: Article[] = [];
  private myOpenEdits: Edit[] = [];
  private allOpenEdits: Edit[] = [];
  private isContributor = false;
  private isEditor = false;
  private projectId = PROJECT_ID;

  constructor(
    public currentPerson: CurrentPersonService,
    private personApi: PersonApi,
    private projectApi: ProjectApi,
    private articleApi: ArticleApi,
    private editApi: EditApi,
    private modalService: SuiModalService,
    private auth: LoopBackAuth) {
  }

  ngOnInit() {
    this.currentPerson.getPerson()
    .subscribe(person => this.person = person);

    this.currentPerson.isProjectContributor()
    .subscribe(isContrib => this.isContributor = isContrib);

    this.currentPerson.isProjectEditor()
    .subscribe(isEditor => {
      this.isEditor = isEditor;

      if (isEditor) {
        this.projectApi.getArticles(PROJECT_ID, {where: {status: 'draft'}, include: ['contributors']})
        .map(res => <Article[]>res)
        .subscribe(drafts => this.drafts = drafts);

        this.projectApi.getArticles(PROJECT_ID, {where: {status: 'public-draft'}, include: ['contributors']})
        .map(res => <Article[]>res)
        .subscribe(publicDrafts => this.publicDrafts = publicDrafts);

        this.projectApi.getEdits(PROJECT_ID, {where: {finished: false}})
        .map(res => <Edit[]>res)
        .subscribe(edits => this.allOpenEdits = edits);
      }
    });

    this.personApi.getArticles(this.auth.getCurrentUserId(), {include: ['contributors']})
    .map(res => <Article[]>res)
    .subscribe(contributions => this.contributions = contributions);

    this.personApi.myOpenEdits()
    .map(res => <Edit[]>res)
    .subscribe(edits => this.myOpenEdits = edits);
  }

  private deleteArticle(article: Article) {
    this.modalService
    .open(new ConfirmModal('Are you sure?', 'Do you want to delete \'' + article.title + '\'?'))
    .onApprove(() => {
      this.projectApi.destroyByIdArticles(PROJECT_ID, article.id)
      .subscribe(() => {
        this.contributions = this.contributions.filter(a => a.id !== article.id);
        this.drafts = this.drafts.filter(a => a.id !== article.id);
        this.publicDrafts = this.publicDrafts.filter(a => a.id !== article.id);
      }, error => {
        if (error) this.modalService.open(new ErrorModal(error.statusCode, error.message));
      });
    });
  }

  private deleteEdit(edit: Edit) {
    this.modalService
    .open(new ConfirmModal('Are you sure?', 'Do you want to delete the last edit to \'' + edit.title + '\'?'))
    .onApprove(() => {
      this.editApi.deleteById(edit.id)
      .subscribe(() => {
        this.myOpenEdits = this.myOpenEdits.filter(e => e.id !== edit.id);
        this.allOpenEdits = this.allOpenEdits.filter(e => e.id !== edit.id);
      })
    });
  }

  private daysSince(dateString) {
    var date = new Date(dateString);
    var today = new Date(Date.now());

    return Math.floor((today.getTime() - date.getTime()) / (1000 * 3600 * 24));
  }
}
