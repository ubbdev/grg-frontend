import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Person } from '../shared/sdk/models';
import { CurrentPersonService } from '../shared/sign-in/current-person.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  public person: Person = new Person;
  public title = 'Dashboard';
  private isAdmin = false;
  private isEditor = false;

  constructor(
    public currentPerson: CurrentPersonService,
    private titleService: Title
  ) {
  }

  ngOnInit() {
    this.currentPerson.getPerson()
    .subscribe(person => this.person = person);

    this.currentPerson.isProjectAdmin()
    .subscribe(isAdmin => this.isAdmin = isAdmin);

    this.currentPerson.isProjectEditor()
    .subscribe(isEditor => this.isEditor = isEditor);

    this.titleService.setTitle(this.title);
  }
}
