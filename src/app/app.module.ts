import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { FormsModule } from '@angular/forms';
import { SuiModule } from 'ng2-semantic-ui';

import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ArticleModule } from './article/article.module';
import { SubjectModule } from './subject/subject.module';
import { BibliographyModule } from './bibliography/bibliography.module';

import { WorkListComponent } from './work/work-list/work-list.component';
import { WorkDetailComponent } from './work/work-detail/work-detail.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';

import { SDKBrowserModule, InternalStorage, StorageBrowser } from './shared/sdk/index';

import { AppRoutingModule } from './app-routing.module';
import { SignInComponent } from './shared/sign-in/sign-in.component';
import { PassportComponent } from './shared/sign-in/passport.component';

import { AuthGuard } from './shared/auth-guard.service';
import { CanDeactivateGuard } from './shared/can-deactivate-guard.service';

import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { AboutComponent } from './pages/about/about.component';
import { HelpComponent } from './pages/help/help.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    WorkListComponent,
    WorkDetailComponent,
    SignInComponent,
    PassportComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    HelpComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    SharedModule,
    FormsModule,
    HttpModule,
    SDKBrowserModule.forRoot({
      provide: InternalStorage,
      useClass: StorageBrowser
    }),
    ArticleModule,
    DashboardModule,
    BibliographyModule,
    SubjectModule,
    AppRoutingModule,
    SuiModule,
  ],
  providers: [
    AuthGuard,
    CanDeactivateGuard,
    Title,
  ],
  entryComponents : [ AppComponent ],
  bootstrap       : [ AppComponent ]
})

export class AppModule {}
