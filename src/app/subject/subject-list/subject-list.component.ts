import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Http, Headers, Response } from '@angular/http';
import { PROJECT_ID, PREF_LANG } from '../../shared/project.config';
import { CurrentPersonService } from '../../shared/sign-in/current-person.service';
import { LoopBackAuth } from '../../shared/sdk/services';
import { Concept } from '../../shared/sdk/models';
import { ConceptApi, ProjectApi } from '../../shared/sdk/services';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { getDisplayLabel } from '../subject-util'

@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {
  private concepts: Concept[] = [];
  private personCanEdit = false;
  private error = null;

  constructor(
    private conceptApi: ConceptApi,
    private projectApi: ProjectApi,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private auth: LoopBackAuth,
    private currentPerson: CurrentPersonService
    ) {
  }

  ngOnInit() {
    this.projectApi.getConcepts(PROJECT_ID, {
      where: {parent: null},
      include: [
        'articles',
        'references'
      ],
    })
    .map(res => <Concept[]>res)
    .subscribe(concepts => this.concepts = concepts,
      error => this.error = error,
      () => this.concepts.sort((a, b) => {
        return a.prefLabel.find(l => l.lang === PREF_LANG).name.localeCompare(
          b.prefLabel.find(l => l.lang === PREF_LANG).name);

      })
    );

    this.currentPerson.isProjectEditor()
    .subscribe(isEditor => this.personCanEdit = isEditor);
  }
}
