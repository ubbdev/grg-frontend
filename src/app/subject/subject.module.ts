import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';

import { SubjectComponent } from './subject.component';
import { SubjectListComponent } from './subject-list/subject-list.component';
import { SubjectDetailComponent } from './subject-detail/subject-detail.component';

import { SubjectRoutingModule } from './subject-routing.module';
import { SubjectCardComponent } from './subject-card/subject-card.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    SubjectRoutingModule,
  ],
  declarations: [
    SubjectComponent,
    SubjectListComponent,
    SubjectDetailComponent,
    SubjectCardComponent,
  ],
  providers: [  ]
})
export class SubjectModule {}
