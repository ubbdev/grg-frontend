import { Concept } from '../shared/sdk/models';
import { PREF_LANG } from '../shared/project.config';

export function getDisplayLabel(concept: Concept) {
  return concept.prefLabel.find(l => l.lang === PREF_LANG).name;
}
