import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, UrlSegment } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { PROJECT_ID, PREF_LANG, PREF_LANG_LONG } from '../../shared/project.config';
import { CurrentPersonService } from '../../shared/sign-in/current-person.service';
import { ErrorModal } from '../../shared/messages/error-modal.component';
import { ConfirmModal } from '../../shared/messages/confirm-modal.component';
import { ConceptApi, ProjectApi } from '../../shared/sdk/services';
import { ReferenceSource } from '../../shared/search-sources'
import { Concept, Reference } from '../../shared/sdk/models';
import { getDisplayLabel } from '../subject-util';
import { Observable } from 'rxjs/Observable';
import { SuiModalService } from 'ng2-semantic-ui';

@Component({
  selector: 'app-subject-detail',
  templateUrl: './subject-detail.component.html',
  styleUrls: ['./subject-detail.component.css']
})
export class SubjectDetailComponent implements OnInit {
  private isNewConcept = false;
  private isNewChild = false;
  private concept: Concept = null;
  private parent: Concept = null;
  private prefLabel: any = null;
  private altLabels = [];
  private editedPrefLabel: any = null;
  private editedAltLabel: any = null;
  private editedAltLabelIndex = -1;
  private personCanEdit = false;
  private editedDefinition = null;
  private editedScopeNote = null;
  private editingParent = false;
  private selectedParent = null;
  private searchResults: Concept[] = null;
  private error: Error = null; // mainly for 404s
  private children: Concept[] = [];
  private prefLangLabel = PREF_LANG_LONG;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private conceptApi: ConceptApi,
    private projectApi: ProjectApi,
    private titleService: Title,
    private currentPerson: CurrentPersonService,
    private modalService: SuiModalService,
    private referenceSource: ReferenceSource
    ) { }

  ngOnInit() {
    Observable.combineLatest(
      this.route.url,
      this.route.paramMap,
    )
    // get concept
    .switchMap(config => {
      const urlSegment = config[0];
      const paramMap = config[1];
      if (urlSegment[0].path === 'create') {
        this.isNewConcept = true;
        this.editedPrefLabel = {name: '', lang: PREF_LANG};
        return Observable.of(new Concept({prefLabel: [this.editedPrefLabel]}));
      } else if (urlSegment[1] && urlSegment[1].path === 'addChild') {
        this.isNewConcept = true;
        this.isNewChild = true;
        this.editedPrefLabel = {name: '', lang: PREF_LANG};
        return Observable.of(new Concept({prefLabel: [this.editedPrefLabel], parent: paramMap.get('id')}));
      }
      return this.conceptApi.findById(paramMap.get('id'), { include: ['articles', 'references'] });
    })
    .map(res => <Concept>res)
    .map(concept => {
      this.concept = concept;
      this.parent = null;
      this.prefLabel = concept.prefLabel.find(l => l.lang === PREF_LANG);

      if (concept.altLabel) {
        this.altLabels = concept.altLabel.filter(l => l.lang === PREF_LANG);
      } else {
        this.altLabels = null;
      }

      if (!this.isNewConcept) {
        this.titleService.setTitle(this.prefLabel.name + ' | ' + PROJECT_ID);
      } else {
        this.titleService.setTitle('New concept | ' + PROJECT_ID);
      }

      if (!this.concept.references) {
        this.concept.references = [];
      }
    })
    // get parent
    .flatMap(() => {
      if (this.concept.parent) return this.conceptApi.findById(this.concept.parent);
      return Observable.of(null);
    })
    .map(res => { // prevent the flow from halting here (perhaps a forkJoin or something would be better)
      if (res) return <Concept>res;
      return null;
    })
    .map(parent => this.parent = parent)
    // get children
    .flatMap(() => {
      return this.projectApi.getConcepts(PROJECT_ID, {where: {parent: this.concept.id}});
    })
    .map(res => <Concept[]>res)
    .map(children => {
      this.children = children;
      this.children.sort((a, b) => {
        return a.prefLabel.find(l => l.lang === PREF_LANG).name.localeCompare(
          b.prefLabel.find(l => l.lang === PREF_LANG).name);
      });
    })
    .subscribe(() => { }, error => this.error = error);

    this.referenceSource.init();

    this.currentPerson.isProjectEditor()
    .subscribe(isEditor => this.personCanEdit = isEditor);
  }

  getLabel(concept) {
    return getDisplayLabel(concept);
  }

  private saveChanges(): Observable<Concept> {
    let obs = null;

    if (!this.isNewConcept) {
      obs = this.conceptApi.patchAttributes(this.concept.id, this.concept);
    } else if (this.isNewChild) {
      obs = this.conceptApi.addChild(this.concept.parent, null, this.concept);
    } else {
      obs = this.projectApi.createConcepts(PROJECT_ID, this.concept)
    }

    return obs.map(res => <Concept>res)
    .map(concept => {
      if (this.isNewConcept) this.router.navigate(['subjects/', concept.id])
    })
    .catch(error => {
      this.modalService.open(new ErrorModal(error.statusCode, error.message));
      Observable.throw(error);
    });
  }

  private editPrefLabel() {
    this.editedPrefLabel = {
      name: this.prefLabel.name,
      lang: this.prefLabel.lang
    };
  }

  private savePrefLabel() {
    this.concept.prefLabel.forEach((label, index) => {
      if (label.lang === this.editedPrefLabel.lang) {
        this.concept.prefLabel[index] = this.editedPrefLabel;
      }
    });
    this.prefLabel = {
      name: this.editedPrefLabel.name,
      lang: this.editedPrefLabel.lang,
    };
    this.saveChanges()
    .subscribe(
      () => { this.editedPrefLabel = null },
      err => { console.log(err); });
  }

  private editAltLabel(label: any, index: number) {
    if (label) {
      this.editedAltLabel = {
        name: label.name,
        lang: PREF_LANG,
      };
    } else {
      this.editedAltLabel = {
        name: '',
        lang: PREF_LANG,
      }
    }
    this.editedAltLabelIndex = index;
  }

  private saveAltLabel() {
    let newAltLabel = {
      name: this.editedAltLabel.name,
      lang: this.editedAltLabel.lang
    };

    if (this.editedAltLabelIndex >= 0) {
      this.concept.altLabel[this.editedAltLabelIndex] = newAltLabel;
    } else {
      if (!this.concept.altLabel) this.concept.altLabel = [];
      if (!this.altLabels) this.altLabels = [];
      this.concept.altLabel.push(newAltLabel);
      this.altLabels.push(newAltLabel);
      this.editedAltLabelIndex = this.concept.altLabel.length - 1;
    }
    this.saveChanges()
    .subscribe(() => {
      this.altLabels[this.editedAltLabelIndex] = newAltLabel;
      this.editedAltLabel = this.editedAltLabelIndex = null;
    }, err => {});
  }

  private removeAltLabel(index: number) {
    const altLabelToRemove = this.concept.altLabel[index];
    this.concept.altLabel.splice(index, 1);
    this.saveChanges()
    .subscribe(() => {
      this.altLabels.splice(index, 1);
    }, err => {
      this.concept.altLabel.splice(index, 0, altLabelToRemove);
    });
  }

  private editDefinition() {
    this.editedDefinition = "";
    if (this.concept.definition) this.editedDefinition = this.concept.definition;
  }

  private saveDefinition() {
    const oldDefinition = this.concept.definition;
    this.concept.definition = this.editedDefinition;
    this.saveChanges()
    .subscribe(
      () => { this.editedDefinition = null; },
      err => { this.concept.definition = oldDefinition; }
    );
  }

  private editScopeNote() {
    this.editedScopeNote = "";
    if (this.concept.scopeNote) this.editedScopeNote = this.concept.scopeNote;
  }

  private saveScopeNote() {
    const oldScopeNote = this.concept.scopeNote;
    this.concept.scopeNote = this.editedScopeNote;
    this.saveChanges()
    .subscribe(
      () => { this.editedScopeNote = null; },
      err => { this.concept.scopeNote = oldScopeNote; }
    );
  }

  private editParent() {
    this.editingParent = true;
  }

  private findConcept = (searchString: string): Promise<string[]> => {
    return this.projectApi.getConcepts(PROJECT_ID, {
      where: {
        'prefLabel.name': {like: '.*' + searchString + '.*', options: 'i'},
        'prefLabel.lang': PREF_LANG}
      })
    .map(res => <Concept[]>res)
    .map(concepts => {
      this.searchResults = concepts;
      return concepts.map(c => c.prefLabel[0].name);
    })
    .toPromise();
  }

  private selectParent(conceptName: string) {
    this.selectedParent = this.searchResults.find(c => c.prefLabel.some(l => l.name === conceptName))
  }

  private cancelMove() {
    this.editingParent = false;
    this.selectedParent = null;
  }

  private makeConceptRootNode() {
    this.selectedParent = null;
    this.moveNode();
  }

  private moveNode() {
    const target = this.selectedParent;

    let obs = null;
    if (target !== null) obs = this.conceptApi.moveNode(this.concept.id, target.id);
    else obs = this.conceptApi.makeNodeRoot(this.concept.id);

    obs.map(res => {
      this.concept.parent = target ? target.id : null;
      this.parent = target;
      this.editingParent = false;
      this.selectedParent = null;
      return <Concept>res;
    })
    .subscribe(() => {}, error => {
      this.modalService.open(new ErrorModal(error.statusCode, error.message));
    });
  }

  private removeConcept() {
    this.modalService
    .open(new ConfirmModal('Are you sure?', 'Do you want to delete ' + this.prefLabel.name + '?'))
    .onApprove(() => {
      this.conceptApi.deleteById(this.concept.id)
      .subscribe(() => {
        this.router.navigate(['subjects']);
      }, error => {
        this.modalService.open(new ErrorModal(error.statusCode, error.message));
      });
    });
  }

  conceptHasReference(reference: Reference): boolean {
    return this.concept.references && this.concept.references.some(r => r.id === reference.id);
  }

  addReference(reference: Reference) {
    this.concept.references.push(reference);
    this.conceptApi.linkReferences(this.concept.id, reference.id)
    .subscribe(() => { }, error => {
      this.modalService.open(new ErrorModal(error.statusCode, error.message));
    });
  }

  removeReference(reference: Reference) {
    this.concept.references = this.concept.references.filter(r => r.id !== reference.id);
    this.conceptApi.unlinkReferences(this.concept.id, reference.id)
    .subscribe(() => { }, error => {
      this.modalService.open(new ErrorModal(error.statusCode, error.message));
    });
  }
}
