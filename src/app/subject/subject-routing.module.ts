import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubjectComponent } from './subject.component';
import { SubjectListComponent } from './subject-list/subject-list.component';
import { SubjectDetailComponent } from './subject-detail/subject-detail.component';

import { AuthGuard } from '../shared/auth-guard.service';

const subjectRoutes: Routes = [
  {
    path: 'subjects',
    component: SubjectComponent,
    children: [
      {
        path: '',
        component: SubjectListComponent
      },
      {
        path: 'create',
        component: SubjectDetailComponent,
        canActivate: [AuthGuard],
      },
      {
        path: ':id',
        component: SubjectDetailComponent
      },
      {
        path: ':id/addChild',
        component: SubjectDetailComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
];
@NgModule({
  imports: [ RouterModule.forChild(subjectRoutes) ],
  exports: [ RouterModule ]
})
export class SubjectRoutingModule {}
