import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PROJECT_ID } from '../shared/project.config';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
     this.titleService.setTitle('Subjects | ' + PROJECT_ID);
  }

}
