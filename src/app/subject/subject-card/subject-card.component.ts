import { Component, OnInit, Input } from '@angular/core';
import { getDisplayLabel } from '../subject-util';
import { Concept } from '../../shared/sdk/models'

@Component({
  selector: 'app-subject-card',
  templateUrl: './subject-card.component.html',
  styleUrls: ['./subject-card.component.css']
})
export class SubjectCardComponent implements OnInit {
  @Input()
  concept: Concept;

  constructor() { }

  ngOnInit() {
  }

  getLabel(concept) {
    return getDisplayLabel(concept);
  }
}
