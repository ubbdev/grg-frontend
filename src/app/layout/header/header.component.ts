import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PROJECT_TITLE } from '../../shared/project.config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title = PROJECT_TITLE;

  constructor(
    public router: Router) { }

  ngOnInit() {
  }

}
