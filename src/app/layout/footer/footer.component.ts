import { Component, OnInit } from '@angular/core';
import { PROJECT_TITLE } from '../../shared/project.config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  title = PROJECT_TITLE;

  constructor() { }

  ngOnInit() {
  }

}
