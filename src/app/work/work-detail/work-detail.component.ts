import { Component, OnInit, Sanitizer } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Work } from '../../shared/sdk/models';
import { WorkApi } from '../../shared/sdk/services';
import 'rxjs/add/operator/switchMap';
import 'rxjs/Rx';
import { DomSanitizer, SafeResourceUrl, SafeHtml } from '@angular/platform-browser';


@Component({
  selector: 'app-work-detail',
  templateUrl: './work-detail.component.html',
  styleUrls: ['./work-detail.component.css']
})
export class WorkDetailComponent implements OnInit {

  private error: Error = null;
  private work: Work;

  constructor(
    private sanitizer: DomSanitizer,
    private workApi: WorkApi,
    private route: ActivatedRoute,
    ) {
  }

  ngOnInit() {
    this.route.params
    .switchMap((params: Params) => this.workApi.findById(params['id']))
    .map(res => <Work>res)
    .subscribe(work => {
      this.work = work;
    }, error => { this.error = error; });
  }
}
