import { Component, OnInit } from '@angular/core';
import { WorkSource } from '../../shared/search-sources/';

@Component({
  selector: 'app-work-list',
  templateUrl: './work-list.component.html',
  styleUrls: ['./work-list.component.css']
})

export class WorkListComponent implements OnInit {

  constructor(private workSource: WorkSource) {}

  ngOnInit() {
    this.workSource.limit = 20;
    this.workSource.init();

  }
}
