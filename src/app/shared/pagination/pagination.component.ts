import {Component, OnInit, EventEmitter, Output, Input, Renderer, ElementRef} from '@angular/core';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit{

  @Input()
  total = 0;

  @Input()
  page = 0;

  @Input()
  pageSize = 10;

  @Output()
  goTo: EventEmitter<number> = new EventEmitter<number>();

  @Input()
  nBoxes = 10;

  constructor(private element: ElementRef, private renderer: Renderer) { }

  ngOnInit() {
    if (this.nBoxes % 2 !== 0) {
      this.renderer.setElementStyle(this.element.nativeElement, 'display', 'none');
      throw Error('nBoxes must be even number');
    }
  }

  lastPage() {
    return Math.floor(this.total / this.pageSize);
  }

  pagesRange() {
    let startPage: number, endPage: number;
    const lastPage = this.lastPage();
    if (lastPage <= this.nBoxes) {
      // less than 10 total pages so show all
      startPage = 0;
      endPage = lastPage;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (this.page <= this.nBoxes / 2 + 1) {
        startPage = 0;
        endPage = this.nBoxes;
      } else if (this.page + this.nBoxes / 2 - 1 >= lastPage) {
        startPage = lastPage - this.nBoxes + 1;
        endPage = lastPage;
      } else {
        startPage = this.page - this.nBoxes / 2;
        endPage = this.page + this.nBoxes / 2 - 1;
      }
    }
    return this.range(startPage, endPage + 1);
  }

  prevPage() {
    return Math.max(0, this.page - 1);
  }

  nextPage() {
    return Math.min(this.lastPage(), this.page + 1);
  }

  pageClicked(page: number) {
    this.goTo.next(page);
  }

  range(start, stop, step = 1) {
    if (!stop) {
      start = 0;
      stop = start;
    }
    return Array.from(new Array(Number((stop - start) / step)), (x, i) => start + i * step);
  }
}
