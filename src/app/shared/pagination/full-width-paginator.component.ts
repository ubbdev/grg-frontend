import {Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-full-width-paginator',
  templateUrl: './full-width-paginator.component.html'
})
export class FullWidthPaginatorComponent implements OnInit {
  @Input() source: any;
  @Input() extraClass: string;

  constructor() {}

  ngOnInit() {}
}
