import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoopBackAuth } from '../shared/sdk/services';
import { PersonApi } from '../shared/sdk/services';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private personApi: PersonApi,
    private auth: LoopBackAuth
  ) {}

  canActivate(): boolean {
    return this.checkLogin();
  }

  canActivateChild(): boolean {
    return this.canActivate();
  }

  checkLogin(): boolean {
    if (this.personApi.isAuthenticated()) {
      return true;
    } else {
      // Navigate to the login page with extras
      this.router.navigate(['/sign-in']);
      return false;
     }
  }
}
