import { Injectable } from '@angular/core';
import { PROJECT_ID } from '../project.config';
import { ProjectApi } from '../sdk/services';
import { Concept } from '../sdk/models';
import { SearchSource } from './search-source';

@Injectable()
export class ConceptSource extends SearchSource<Concept> {
  constructor(private projectApi: ProjectApi) {
    super(
      (searchString, pageNumber) => {
        return this.projectApi.getConcepts(PROJECT_ID, {
          where: {
            'prefLabel.name': { like: '.*' + searchString + '.*', options: 'i'}
          },
          skip: pageNumber * this.limit,
          limit: this.limit,
        });
      },
      searchString => {
        return this.projectApi.countConcepts(PROJECT_ID, {'prefLabel.name': { regex: searchString }});
      }
    );
  }
}
