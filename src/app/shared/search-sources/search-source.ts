import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/switchMap';

export class SearchSource<T> {
  public items$: Observable<T[]>;
  public terms = '';
  public searchTermStream = new Subject<string>();
  public page = 0;
  public pageStream = new Subject<number>();

  public searchCallback: (query: string, pageNumber: number) => Observable<any>;
  public countCallback: (query: string) => Observable<any>;

  public count = 0;
  public countStream = new Subject<any>();
  public total$: Observable<number>;

  public limit = 10;

  // searchCallBack should take arguments searchString and pageNumber
  constructor(searchCallback, countCallback) {
    this.searchCallback = searchCallback;
    this.countCallback = countCallback;
  }

  public init() {
    this.terms = '';

    const searchSource = this.searchTermStream
    .debounceTime(200)
    .distinctUntilChanged()
    .map(searchTerm => {
      this.terms = searchTerm;
      return {search: searchTerm, page: 0};
    });

    const pageSource = this.pageStream.map(pageNumber => {
      this.page = pageNumber;
      return {search: this.terms, page: pageNumber};
    });

    this.items$ = pageSource
    .merge(searchSource)
    .startWith({search: this.terms, page: this.page})
    .switchMap((params: {search: string, page: number}) => {
      return this.searchCallback(params.search, params.page);
    })
    .map(res => <T[]>res)
    .share();

    const countSource = this.countStream.map(countNumber => {
      this.count = countNumber;
      return {search: this.terms, page: countNumber};
    });

    const count = countSource
      .merge(searchSource)
      .startWith({search: this.terms})
      .switchMap((params: {search: string}) => {
        return this.countCallback(params.search);
      })
      .share();

    this.total$ = count.pluck('count');
  }

  search(terms: string) {
    this.searchTermStream.next(terms);
  }

  goToPage(page: number) {
    this.pageStream.next(page);
  }
}
