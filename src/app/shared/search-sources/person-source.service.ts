import { Injectable } from '@angular/core';
import { PROJECT_ID } from '../project.config';
import { Project, Person } from '../sdk/models';
import { SearchSource } from './search-source';
import { ProjectApi } from '../sdk/services';


@Injectable()
export class PersonSource extends SearchSource<Person> {
  private genInclude = function(searchString) {
    return {
      relation: 'people',
      scope: {
        where: {
          name: {regexp: '/' + searchString + '.*/i'}
        }
      }
    };
  }

  constructor(private projectApi: ProjectApi) {
    super(
      (searchString, pageNumber) => {
        return this.projectApi.findById(PROJECT_ID,
          {
            include: this.genInclude(searchString),
            skip: pageNumber * this.limit,
            limit: this.limit,
          })
        .map((res: Project) => {
          return res.people;
        });
      },
      searchString => {
        return this.projectApi.findById(PROJECT_ID, {include: this.genInclude(searchString)})
        .map((res: Project) => {
          return res.people.length;
        });
      }
    );
  }
}
