import { Injectable } from '@angular/core';
import { PROJECT_ID } from '../project.config';
import { ProjectApi } from '../sdk/services';
import { Reference } from '../sdk/models';
import { ElasticSource } from './elastic-source';

@Injectable()
export class ReferenceSource extends ElasticSource<Reference> {
  private selectedCollectionKeys = new Set<string>();

  constructor(private projectApi: ProjectApi) {
    super(
      (searchString: string, pageNumber: number) => {
        if (this.selectedCollectionKeys.size > 0) {
          searchString += ' collections:' + Array.from(this.selectedCollectionKeys).join('+');
        }

        let query = searchString.length === 0 ?
          { match_all: { } } :
          {
            query_string: {
              default_operator: 'AND',
              query: searchString
            }
          }

        return this.projectApi.searchReferencesES(PROJECT_ID, {
          from: pageNumber * this.limit,
          size: this.limit,
          aggs: {
            collections: {
              terms: {
                field: 'collections',
                size: 100,
              }
            }
          },
          query: query
        }, null);
      },
    );
  }

  toggleCollection(key: string): void {
    if (this.collectionIsSelected(key)) {
      this.selectedCollectionKeys.delete(key);
    }
    else {
      this.selectedCollectionKeys.add(key);
      this.search(this.terms);
    }
  }

  collectionIsSelected(key: string): boolean {
    return this.selectedCollectionKeys.has(key);
  }
}
