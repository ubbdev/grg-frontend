import { Injectable } from '@angular/core';
import { PROJECT_ID } from '../project.config';
import { ProjectApi } from '../sdk/services';
import { Article } from '../sdk/models';
import { ElasticSource } from './elastic-source';

@Injectable()
export class ArticleSource extends ElasticSource<Article> {
  constructor(
    private projectApi: ProjectApi,
  ) {
    super(
      (searchString, pageNumber) => {
        let query = searchString.length === 0 ?
          { match_all: { } } :
          {
            query_string: {
              default_operator: 'AND',
              query: searchString
            }
          }
        return this.projectApi.searchArticlesES(PROJECT_ID, {
          from: pageNumber * this.limit,
          size: this.limit,
          query: query
        }, null)
      });
  }
}
