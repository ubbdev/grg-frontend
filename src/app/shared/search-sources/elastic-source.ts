import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/switchMap';

export class ElasticHits<T> {
  public hits: T[];
  public total: number;
  public aggregations: any;
}

export class ElasticSource<T> {
  public result$: Observable<ElasticHits<T>>;
  public total$: Observable<number>;
  public items$: Observable<T[]>;
  public aggregations$: Observable<any>;
  public waiting = new Subject<boolean>();

  public terms = '';
  public page = 0;
  public limit = 10;

  protected searchTermStream = new Subject<string>();
  protected pageStream = new Subject<number>();

  protected searchCallback: (query: string, pageNumber: number) => Observable<ElasticHits<T>>;

  // searchCallBack should take arguments searchString and pageNumber
  constructor(searchCallback) {
    this.searchCallback = searchCallback;
  }

  public init() {
    this.terms = '';

    const searchSource = this.searchTermStream
    .debounceTime(300)
    .map(searchTerm => {
      this.waiting.next(true);
      this.terms = searchTerm;
      return {search: searchTerm, page: 0};
    });

    const pageSource = this.pageStream.map(pageNumber => {
      this.page = pageNumber;
      return {search: this.terms, page: pageNumber};
    });

    this.result$ = pageSource
    .merge(searchSource)
    .startWith({search: this.terms, page: this.page})
    .switchMap((params: {search: string, page: number}) => {
      return this.searchCallback(params.search, params.page);
    })
    .map(res => <ElasticHits<T>>res)
    .do(() => this.waiting.next(false))
    .shareReplay(1);

    this.waiting.publish();

    this.total$ = this.result$.map(res => { res.total; return res.total;});
    this.items$ = this.result$.map(res => res.hits);
    this.aggregations$ = this.result$.map(res => res.aggregations);
  }

  search(terms: string) {
    this.searchTermStream.next(terms);
  }

  goToPage(page: number) {
    this.pageStream.next(page);
  }
}
