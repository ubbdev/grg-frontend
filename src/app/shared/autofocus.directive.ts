import { Directive, ElementRef, Renderer, Input, OnInit } from '@angular/core';

@Directive({
    selector: '[appAutofocus]'
})
export class AutofocusDirective implements OnInit{

  private _autofocus;

  constructor(
    private el: ElementRef,
    private renderer: Renderer) {}

  ngOnInit() {
    if (this._autofocus || typeof this._autofocus === 'undefined') {
      this.renderer.invokeElementMethod(this.el.nativeElement, 'focus', []);
    }
  }

  @Input()
  set appAutofocus(condition: boolean) {
    this._autofocus = condition !== false;
  }
}
