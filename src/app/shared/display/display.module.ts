import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayArticleComponent } from './display-article/display-article.component';
import { RouterModule } from '@angular/router';
import { TruncateModule } from 'ng2-truncate';
import { DisplayReferenceComponent } from './display-reference/display-reference.component';
import { ZoteroCreatorFullNamePipe, ZoteroCreatorLastNamePipe } from './zotero-creator.pipe';
import { DisplayCompactReferenceComponent } from './display-compact-reference/display-compact-reference.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TruncateModule
  ],
  declarations: [
    DisplayArticleComponent,
    DisplayReferenceComponent,
    ZoteroCreatorFullNamePipe,
    ZoteroCreatorLastNamePipe,
    DisplayCompactReferenceComponent
  ],
  exports: [
    DisplayArticleComponent,
    DisplayReferenceComponent,
    DisplayCompactReferenceComponent,
    ZoteroCreatorFullNamePipe,
    ZoteroCreatorLastNamePipe
  ]
})
export class DisplayModule { }
