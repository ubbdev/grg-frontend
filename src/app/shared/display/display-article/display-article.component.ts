import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Article } from '../../sdk/models';
import { ArticleApi } from '../../sdk/services';

@Component({
  selector: 'app-display-article',
  templateUrl: './display-article.component.html',
  styleUrls: ['./display-article.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DisplayArticleComponent implements OnInit {
  @Input() article: Article = null;
  @Input() articleId: string = null

  private contributors: string = null;

  constructor(private articleApi: ArticleApi) { }

  ngOnInit() {
    if (this.articleId) {
      this.articleApi.findById(this.articleId, {include: 'contributors'})
      .map(res => <Article>res)
      .catch((err, caught) => {
        if (err.statusCode === 401) {
          return Observable.of(null)
        } else {
          throw err;
        }
      })
      .subscribe(article => {
        if (article) {
          this.article = article;
          this.contributors = article.contributors.map(c => c.name).join(', ');
        }
      }, err => console.log(err));
    } else if (this.article) {
      this.contributors = this.article.contributors.map(c => c.name).join(', ');
    }
  }
}
