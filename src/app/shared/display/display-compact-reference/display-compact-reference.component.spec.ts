import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayCompactReferenceComponent } from './display-compact-reference.component';

describe('DisplayCompactReferenceComponent', () => {
  let component: DisplayCompactReferenceComponent;
  let fixture: ComponentFixture<DisplayCompactReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayCompactReferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayCompactReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
