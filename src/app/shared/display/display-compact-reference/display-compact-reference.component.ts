import { Component, OnInit, Input } from '@angular/core';
import { Reference } from '../../sdk/models';

@Component({
  selector: 'app-display-compact-reference',
  templateUrl: './display-compact-reference.component.html',
  styleUrls: ['./display-compact-reference.component.css']
})
export class DisplayCompactReferenceComponent implements OnInit {
  @Input() reference: Reference = null;

  constructor() { }

  ngOnInit() {
  }

  formatDate(date): string {
    return date.replace(/^.*(\d\d\d\d).*$/, '$1');
  }

  formatSerialPublication(reference): string {
    let result = '';

    if (reference.publicationTitle) {
      result += '<em>' + reference.publicationTitle.replace(/\.$/,'') + '</em>';
    }

    if (reference.series) {
      result += 'In series: <em>' + reference.series + '</em>';
    }

    if ((reference.publicationTitle || reference.series) && reference.volume) {
      result += ', vol. ' + reference.volume;
      if (reference.issue) {
        result += ' ' + reference.issue;
      }
      if (reference.pages) {
        result += ' p. ' + reference.pages;
      }
    }
    result += '.';

    return result;
  }

}
