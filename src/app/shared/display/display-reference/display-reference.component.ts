import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Reference } from '../../sdk/models';
import { getDisplayLabel} from '../../../subject/subject-util';

@Component({
  selector: 'app-display-reference',
  templateUrl: './display-reference.component.html',
  styleUrls: ['./display-reference.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DisplayReferenceComponent implements OnInit {
  @Input() reference: Reference = null;
  @Input() options = { showAbstract: true };

  constructor() { }

  ngOnInit() { }

  getConceptLabel(concept) {
    return getDisplayLabel(concept);
  }
}
