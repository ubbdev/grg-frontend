import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'zoteroCreatorFullNames'
})

@Injectable()
export class ZoteroCreatorFullNamePipe implements PipeTransform {
  transform(items: any[]): string {
    if (!items) return '';

    // Probably cases missing, will extend as we go
    return items
    .filter(creator => creator.match(/^[^\(]+$|\((author|ed.*)\)/i))
    .map(creator =>
      creator
      .replace(/, \(.*\)/, '') // Doe, John, (author)
      .replace(/\(.*\)/, '') // Doe, John (author)
      .replace(/\ +$/,'') // Doe, John, (Author) (Ed.)
      .replace(/\./,'') // Doe, J. => Doe, J
    ).join('; ');
  }
}


@Pipe({
  name: 'zoteroCreatorLastNames'
})

@Injectable()
export class ZoteroCreatorLastNamePipe implements PipeTransform {
  transform(items: any[]): string {
    if (!items) return '';

    // Probably cases missing, will extend as we go
    return items.map(creator =>
      creator
      .replace(/, (.*)/g, '') // Doe, John, (author) -> Doe
    ).join('; ');
  }
}
