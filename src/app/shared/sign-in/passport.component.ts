import { Component } from '@angular/core';
import { SDKToken } from '../sdk/models';
import { LoopBackAuth } from '../sdk/services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './passport.component.html',
  styleUrls: ['./passport.component.css']
})

export class PassportComponent {

  constructor(
    private auth: LoopBackAuth,
    private route: ActivatedRoute,
    private router: Router
    ) {
    this.route.params.subscribe((token: SDKToken) => {
      if (token.id && token.userId) {
        this.auth.setToken(token);
        this.auth.setRememberMe(true);
        this.auth.save();
        this.router.navigate(['/dashboard']);
      }
    });
  }

}
