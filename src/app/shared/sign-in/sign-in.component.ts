import { Component } from '@angular/core';
import { BASE_URL, PROJECT_ID } from '../project.config';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

export class SignInComponent {
    login() {
      window.location.href = BASE_URL + '/auth/' + PROJECT_ID + '/dataporten';
    }
}
