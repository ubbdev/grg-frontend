import { Injectable } from '@angular/core';
import { LoopBackAuth } from '../sdk/services';
import { PersonApi } from '../sdk/services';
import { PROJECT_ID } from '../project.config';
import { Person } from '../sdk/models';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class CurrentPersonService {
  private person: Person = null;
  private roles: Array<string> = [];

  constructor(
    private personApi: PersonApi,
    private auth: LoopBackAuth
    ) { }

  // Returns empty observable, needs to be called on root element
  public init(): Observable<any> {
    if (this.auth.getCurrentUserId() === null) return Observable.empty();

    return this.personApi.findById(this.auth.getCurrentUserId())
    .map(person => this.person = <Person>person)
    .flatMap(() => this.personApi.myRoles(this.auth.getCurrentUserId()))
    .map(res => {
      this.roles = [];
      res.forEach(role => { if (role.projectId === PROJECT_ID) this.roles.push(role.role); });
    });
  }

  public getPerson(): Observable<Person> {
    if (!this.person) {
      return this.init().flatMap(() => { return Observable.of(this.person); });
    }
    return Observable.of(this.person);
  }

  public getRoles(): Observable<String[]> {
    if (!this.person) {
      return this.init().flatMap(() => { return Observable.of(this.roles); });
    }
    return Observable.of(this.roles);
  }

  private hasRole(role: string): Observable<boolean> {
    if (!this.person) {
      return this.init().flatMap(() => { return Observable.of(this.roles.includes(role)); });
    }
    return Observable.of(this.roles.includes(role));
  }

  private checkRole(role: string, i: number, array: Array<string>): boolean {
    return this.roles.includes(role);
  }

  private hasOneOfRoles(roles: Array<string>): Observable<boolean> {
    if (!this.roles || this.roles.length === 0) {
      return this.init().flatMap(() => { return Observable.of(roles.some(this.checkRole, this)); });
    }
    return Observable.of(roles.some(this.checkRole, this));
  }

  public isProjectContributor(): Observable<boolean> {
    return this.hasOneOfRoles(['contributor', 'editor', 'admin']);
  }

  public isProjectEditor(): Observable<boolean> {
    return this.hasOneOfRoles(['editor', 'admin']);
  }

  public isProjectAdmin(): Observable<boolean> {
    return this.hasOneOfRoles(['admin']);
  }
}
