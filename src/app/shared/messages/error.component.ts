import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-error-component',
  template: `
<div class="ui header">
  Error: {{title}}
</div>
<p>{{description}}</p>
`
})

export class ErrorComponent {
  @Input() public title: string;
  @Input() public description: string;
}
