import { Component } from '@angular/core';
import { SuiModal, ComponentModalConfig } from 'ng2-semantic-ui';

interface ErrorModalContext {
  code: number;
  message: string;
}

@Component({
  selector: 'app-error-modal',
  template: `
<div class="header">Error ({{modal.context.code}})</div>
<div class="content">
  <p>{{modal.context.message}}</p>
</div>
<div class="actions">
  <button class="ui green button" (click)="modal.approve(undefined)" autofocus>OK</button>
</div>
`
})

export class ErrorModalComponent {
  constructor(public modal: SuiModal<ErrorModalContext, void, void>) {}
}

@Component({
  selector: 'app-error-modal-wrapper',
  template: ''
})

export class ErrorModal extends ComponentModalConfig<ErrorModalContext, void, void> {
  constructor(code: number, message: string) {
    super(ErrorModalComponent, { code, message });

    this.isClosable = false;
    this.transitionDuration = 200;
  }
}
