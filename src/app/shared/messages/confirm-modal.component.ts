import { Component } from '@angular/core';
import { SuiModal, ComponentModalConfig } from 'ng2-semantic-ui';

interface ConfirmModalContext {
  title: string;
  question: string;
}

@Component({
  selector: 'app-confirm-modal-component',
  template: `
<div class="header">{{ modal.context.title }}</div>
<div class="content">
  <p>{{ modal.context.question }}</p>
</div>
<div class="actions">
  <button class="ui red button" (click)="modal.deny(undefined)">Cancel</button>
  <button class="ui green button" (click)="modal.approve(undefined)" autofocus>OK</button>
</div>
`
})

export class ConfirmModalComponent {
  constructor(public modal: SuiModal<ConfirmModalContext, void, void>) {}
}

@Component({
  selector: 'app-confirm-modal-wrapper',
  template: ''
})
export class ConfirmModal extends ComponentModalConfig<ConfirmModalContext, void, void> {
  constructor(title: string, question: string) {
    super(ConfirmModalComponent, { title, question });

    this.isClosable = false;
    this.transitionDuration = 200;
  }
}
