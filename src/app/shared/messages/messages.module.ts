import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmModal, ConfirmModalComponent } from './confirm-modal.component';
import { ErrorModal, ErrorModalComponent } from './error-modal.component';
import { ErrorComponent } from './error.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ConfirmModal,
    ConfirmModalComponent,
    ErrorModal,
    ErrorModalComponent,
    ErrorComponent
  ],
  entryComponents: [
    ConfirmModalComponent,
    ErrorModalComponent
  ],
  exports: [
    ErrorComponent
  ]
})
export class MessagesModule { }
