import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationComponent } from './pagination/pagination.component';
import { FullWidthPaginatorComponent } from './pagination/full-width-paginator.component';
import { SuiModule } from 'ng2-semantic-ui';
import { MarkdownToHtmlModule } from 'markdown-to-html-pipe';
import { TruncateModule } from 'ng2-truncate';
import { AutofocusDirective } from './autofocus.directive';
import { DisplayModule } from './display/display.module';
import { MessagesModule } from './messages/messages.module';
import { ArticleSource, PersonSource, ReferenceSource, WorkSource, ConceptSource } from './search-sources';
import { CurrentPersonService } from './sign-in/current-person.service';

@NgModule({
  imports:      [ CommonModule, MarkdownToHtmlModule, TruncateModule ],
  declarations: [ PaginationComponent, FullWidthPaginatorComponent, AutofocusDirective ],
  exports:      [ PaginationComponent, FullWidthPaginatorComponent, AutofocusDirective, DisplayModule, MessagesModule,
                  CommonModule, FormsModule, ReactiveFormsModule, SuiModule, MarkdownToHtmlModule, TruncateModule ],
  providers:    [ CurrentPersonService, ArticleSource, PersonSource, ReferenceSource, WorkSource, ConceptSource ]
})
export class SharedModule { }
