import { AfterViewInit, Component, ElementRef, ViewChild, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

declare const require: any;
const SimpleMDE = require('simplemde/dist/simplemde.min.js');

@Component({
  selector: 'app-mdeditor',
  template: '<textarea name="markdowneditor" #simplemde></textarea>',
  styleUrls: ['./mdeditor.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MdeditorComponent),
    multi: true,
  }]
})
export class MdeditorComponent implements AfterViewInit, ControlValueAccessor {
  private data = '';
  private mde: any;

  @ViewChild('simplemde') textarea: ElementRef;

  private propagateChange = (_: any) => { };

  constructor(private elementRef: ElementRef) { }

  ngAfterViewInit() {
    this.mde = new SimpleMDE({
      element: this.textarea.nativeElement,
      forceSync: true,
      status: false
    });

    // can't pass this to mde below
    const pc = this.propagateChange;
    const mde = this.mde;

    mde.codemirror.on('change', function() {
      pc(mde.codemirror.getValue());
    });

    if (this.data) {
      setTimeout(() => {
        mde.codemirror.setValue(this.data);
      }, 0);
    }
  }

  public writeValue(obj: string) {
    if (!obj) return;
    this.data = obj;
    if (this.mde) {
      this.mde.codemirror.setValue(obj);
    }
  }

  public registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  // for touch input, from NG_VALUE_ACCESSOR
  public registerOnTouched() { }
}
