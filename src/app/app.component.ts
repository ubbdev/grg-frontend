import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Person } from './shared/sdk/models';
import { PersonApi } from './shared/sdk/services';
import { LoopBackConfig } from './shared/sdk';
import { BASE_URL, API_VERSION, PROJECT_TITLE, componentConfig } from './shared/project.config';
import { LoopBackAuth } from './shared/sdk/services';
import { CurrentPersonService } from './shared/sign-in/current-person.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  public person: Person = null;
  public title = PROJECT_TITLE;
  public config = componentConfig;

  constructor(
    public router: Router,
    private personApi: PersonApi,
    public auth: LoopBackAuth,
    public currentPerson: CurrentPersonService,
    private titleService: Title,
    ) {
    LoopBackConfig.setBaseURL(BASE_URL);
    LoopBackConfig.setApiVersion(API_VERSION);
  }

  ngOnInit() {
    this.currentPerson.getPerson()
    .subscribe(person => this.person = person);

    this.titleService.setTitle(this.title);

    this.router.events
    .filter(event => event instanceof NavigationStart)
    .subscribe(event => this.titleService.setTitle(this.title));
  }

  logout(): void {
    this.personApi.logout()
      .subscribe(res => {
        this.router.navigate(['/home']);
        this.person = null;
      });
  }
}

