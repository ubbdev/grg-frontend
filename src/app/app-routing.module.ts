import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { HelpComponent } from './pages/help/help.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { WorkListComponent } from './work/work-list/work-list.component';
import { WorkDetailComponent } from './work/work-detail/work-detail.component';

import { SignInComponent } from './shared/sign-in/sign-in.component';
import { PassportComponent } from './shared/sign-in/passport.component';

import { AuthGuard } from './shared/auth-guard.service';

const routes: Routes = [
  { path: 'home',          component: HomeComponent },
  { path: 'about',          component: AboutComponent },
  { path: 'help',          component: HelpComponent },
  { path: 'sign-in', component: SignInComponent, pathMatch: 'full' },
  { path: 'works',         component: WorkListComponent },
  { path: 'works/:id',     component: WorkDetailComponent },
  { path: 'passport/:id/:userId', component: PassportComponent },
  { path: '' , component: HeaderComponent, outlet: 'header'},
  { path: '' , component: FooterComponent, outlet: 'footer'},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**',     component: NotFoundComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
