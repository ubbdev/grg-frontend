import { Component, OnInit, Input } from '@angular/core';
import { Article, PublicComment } from '../../shared/sdk/models';
import { ArticleApi, PublicCommentApi, EditorialCommentApi } from '../../shared/sdk/services';
import { CurrentPersonService } from '../../shared/sign-in/current-person.service';
import { ErrorModal } from '../../shared/messages/error-modal.component';
import { SuiModalService } from 'ng2-semantic-ui';
import { Observable } from 'rxjs/Observable';

// The abstract Comment model from Loopback is hidden in the SDK, here's the elements we need
interface Comment {
  body?: string;
  id?: string;
  children?: Array<any>;
  parent?: string;
  author?: {id?: string, name?: string};
  deleted?: boolean;
}

// This handles most of the logic for both Editorial and Public Comments to articles
abstract class CommentInterface {
  protected comments: Comment[];
  protected article: Article;
  protected articleCreateComment = null;
  constructor(
    protected commentApi,
    protected articleApi: ArticleApi,
    protected currentPersonService: CurrentPersonService,
  ) {
  }

  protected initSuper(comments: Comment[], article: Article): Observable<Comment> {
    this.comments = comments;
    this.article = article;
    return this.loadAllChildren();
  }

  public abstract init(article: Article): Observable<any>;

  protected loadCommentChildren(comment: Comment): Observable<Comment> {
    return this.commentApi.getTree(comment.id, true)
    .map(tree => <Comment>tree)
    .map(tree => {
      comment.children = tree.children;
      return tree;
    });
  }

  public loadAllChildren(): Observable<Comment> {
    return Observable
    .from(this.comments.filter(comment => {
      return comment.children && comment.children.length > 0;
    }))
    .flatMap(comment => {
      return this.loadCommentChildren(comment);
    });
  }

  public canAddComment(): Observable<boolean> {
    return Observable.of(true);
  }

  public canViewComments(): Observable<boolean> {
    return Observable.of(true);
  }

  public canEditComment(comment): Observable<boolean> {
    return this.currentPersonService.getPerson()
    .flatMap(person => {
      if (person && comment.author && comment.author.id === person.id)
        return Observable.of(true);
      return Observable.of(false);
    });
  }



  public canDeleteComment(comment: Comment): Observable<boolean> {
    return this.currentPersonService.getPerson()
    .map(person => {
      if (person && comment.author && comment.author.id === person.id)
        return true;
      return false;
    })
    .flatMap(canRemove => {
      if (canRemove) return Observable.of(true);
      return this.currentPersonService.isProjectEditor();
    });
  }

  public deleteComment(comment: Comment): Observable<Comment> {
    return this.commentApi.patchAttributes(comment.id, {deleted: true})
    .flatMap(() => {
      return this.commentApi.findById(comment.id);
    });
  }

  public canUndeleteComment(comment: Comment): Observable<boolean> {
    return this.currentPersonService.isProjectEditor();
  }

  public undeleteComment(comment: Comment): Observable<Comment> {
    return this.commentApi.patchAttributes(comment.id, {deleted: false})
    .flatMap(() => {
      return this.commentApi.findById(comment.id);
    });
  }

  public abstract submitComment(comment: Comment): Observable<Comment>;

  public submitReply(parentComment: Comment, reply: Comment): Observable<Comment> {
    return this.commentApi.addChild(parentComment.id, null, reply)
    .flatMap(() => {
      return this.loadCommentChildren(parentComment);
    });
  }

  public submitEdit(comment: Comment): Observable<Comment> {
    return this.commentApi.patchAttributes(comment.id, {body: comment.body});
  }
}

// Specialization for public comments to an article
export class ArticlePublicCommentInterface extends CommentInterface {
  public init(article: Article): Observable<any> {
    return this.articleApi.getPublicComments(article.id, {where: {parent: null}, order: 'created ASC'})
    .flatMap((publicComments: Comment[]) => {
      return this.initSuper(publicComments, article);
    })
  }

  public canAddComment(): Observable<boolean> {
    return this.currentPersonService.isProjectContributor();
  }

  public submitComment(comment: Comment): Observable<Comment> {
    return this.articleApi.createPublicComments(this.article.id, comment)
    .flatMap(res => this.loadCommentChildren(res))
    .map(res => {
      this.comments.push(res);
      return res;
    });
  }
}

// Specialization for editorial comments to an article
export class ArticleEditorialCommentInterface extends CommentInterface {
  public init(article: Article): Observable<any> {
    return this.canViewComments()
    .flatMap(canView => {
      if (canView) {
        return this.articleApi.getEditorialComments(article.id, {where: {parent: null}, order: 'created ASC'})
        .flatMap((editorialComments: Comment[]) => {
          return this.initSuper(editorialComments, article);
        });
      }
      return Observable.empty();
    });
  }

  public canAddComment(): Observable<boolean> {
    return this.currentPersonService.isProjectEditor();
  }

  public canViewComments(): Observable<boolean> {
    return this.currentPersonService.isProjectEditor();
  }

  public submitComment(comment: Comment): Observable<Comment> {
    return this.articleApi.createEditorialComments(this.article.id, comment)
    .flatMap(res => this.loadCommentChildren(res))
    .map(res => {
      this.comments.push(res);
      return res;
    });
  }
}

// Component to display comments, takes a CommentInterface as input
@Component({
  selector: 'app-article-comment',
  templateUrl: './article-comment.component.html',
  styleUrls: ['./article-comment.component.css']
})
export class ArticleCommentComponent {
  @Input() public maxDepth = 1;
  @Input() public commentInterface: CommentInterface;

  private newComment: Comment = {body: ""};
  private reply: Comment = null;
  private edit: Comment = null;
  private editError = false;
  private replyError = false;

  constructor(private modalService: SuiModalService) { }

  isNewCommentOpen(): boolean {
    return this.newComment === null;
  }

  private isReplyOpen(parent: Comment): boolean {
    return (this.reply && this.reply.parent === parent.id);
  }

  private openReply(parent: Comment): void {
    if (this.reply) {
      this.replyError = true;
      return;
    }
    if (this.edit) {
      this.editError = true;
      return;
    }
    this.reply = {parent: parent.id, body: ""};
  }

  private submitReply(parent: Comment): void {
    this.commentInterface.submitReply(parent, this.reply)
    .subscribe(res => this.reply = null,
      error => this.modalService.open(new ErrorModal(error.statusCode, error.message)));
  }

  private discardReply(): void {
    this.reply = null;
  }

  private deleteComment(comment: Comment): void {
    this.commentInterface.deleteComment(comment)
    .subscribe(res => {
      comment.author = res.author;
      comment.body = res.body;
      comment.deleted = true;
    }, error => this.modalService.open(new ErrorModal(error.statusCode, error.message)));
  }

  private undeleteComment(comment: Comment): void {
    this.commentInterface.undeleteComment(comment)
    .subscribe(res => {
      comment.author = res.author;
      comment.body = res.body;
      comment.deleted = false;
    }, error => this.modalService.open(new ErrorModal(error.statusCode, error.message)));
  }

  private submitComment(): void {
    this.commentInterface.submitComment(this.newComment)
    .subscribe(res => {
      this.newComment = {body: ""};
      this.edit = null;
    }, error => this.modalService.open(new ErrorModal(error.statusCode, error.message)));
  }

  private isEditOpen(comment: Comment): boolean {
    return (this.edit && this.edit.id === comment.id);
  }

  private openEdit(comment: Comment): void {
    if (this.reply) {
      this.replyError = true;
      return;
    }
    if (this.edit) {
      this.editError = true;
      return
    }
    this.edit = {id: comment.id, body: comment.body};
  }

  private submitEdit(comment: Comment): void {
    this.commentInterface.submitEdit(this.edit)
    .subscribe(res => {
      comment.body = res.body;
      this.edit = null;
    }, error => this.modalService.open(new ErrorModal(error.statusCode, error.message)));
  }

  private discardEdit() {
    this.edit = null;
  }
}
