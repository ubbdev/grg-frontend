import { Component, OnInit, OnChanges, Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationStart } from '@angular/router';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { Http, Headers, Response } from '@angular/http';
import { SharedModule } from '../../shared/shared.module';
import { LoopBackConfig } from '../../shared/sdk/index';
import { LoopBackAuth } from '../../shared/sdk/services';
import { PROJECT_ID, PREF_LANG } from '../../shared/project.config';
import { Article, Edit, Reference, Person, Project, Work, Concept } from '../../shared/sdk/models';
import { ArticleApi, ReferenceApi, PersonApi, ProjectApi, WorkApi, ConceptApi } from '../../shared/sdk/services';
import { PersonSource, ReferenceSource, WorkSource, ConceptSource } from '../../shared/search-sources';
import { CurrentPersonService } from '../../shared/sign-in/current-person.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../shared/messages/confirm-modal.component';
import { ErrorModal } from '../../shared/messages/error-modal.component';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import 'rxjs/Rx';


// Helper class
class SelectorItem {
  public id;
}

// Helper class
class Selector<T extends SelectorItem> {
  public items: T[] = [];

  public select(item: T) {
    this.items.push(item);
  }

  public deselect(item: T) {
    const index: number = this.items.findIndex(i => item.id === i.id);
    if (index >= 0) {
      this.items.splice(index, 1);
    }
  }

  public isSelected(item: T) {
    return this.items.some(i => i.id === item.id);
  }

  public getItems() {
    return this.items;
  }
}

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css'],
})
export class ArticleEditComponent implements OnInit, OnChanges {

  private referenceSelector: Selector<Reference> = new Selector<Reference>();
  private contributorSelector: Selector<Person> = new Selector<Person>();
  private workSelector: Selector<Work> = new Selector<Work>();
  private subjectSelector: Selector<Concept> = new Selector<Concept>();
  private contributors: Person[] = [];
  private subjects: Concept[] = [];
  private relatedWorks: Work[] = [];
  private isEdit = false;
  private isAutoSaving = false;
  private autoSaver: Observable<any> = null;
  private autoSaverSubscription: Subscription;
  private error = null;
  private edit: Edit = new Edit();
  private editForm: FormGroup;
  private loading = true;

  private possibleStatuses = ['draft', 'public-draft',' published'];

  constructor(
    private articleApi: ArticleApi,
    private referenceApi: ReferenceApi,
    private personApi: PersonApi,
    private projectApi: ProjectApi,
    private workApi: WorkApi,
    private conceptApi: ConceptApi,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private auth: LoopBackAuth,
    private fb: FormBuilder,
    private referenceSource: ReferenceSource,
    private personSource: PersonSource,
    private workSource: WorkSource,
    private subjectSource: ConceptSource,
    private currentPersonService: CurrentPersonService,
    private modalService: SuiModalService,
    private titleService: Title,
    ) {
    this.createForm();

    router.events
    .filter(event => event instanceof NavigationStart)
    .subscribe(() => {
      if (this.isEdit && this.autoSaverSubscription) this.autoSaverSubscription.unsubscribe();
    });
  }

  ngOnInit() {
    this.titleService.setTitle('Edit article');

    this.route.params
    .switchMap((params: Params) => {
      if (params['id']) {
        return this.initEdit(params)
        // setup autosaving
        .map(() => {
          this.autoSaver = Observable.interval(15000)
            .do(() => this.isAutoSaving = true)
            .flatMap(() => this.route.params)
            .do(() => this.readEditFromForm())
            .switchMap((params: Params) => this.articleApi.updateByIdEdits(params['id'], this.edit.id, this.edit))
            .delay(500) // just to show the user that we're doing something
            .do(() => this.isAutoSaving = false);
          this.autoSaverSubscription = this.autoSaver.subscribe();
          return true;
        });
      }

      return this.currentPersonService.getPerson()
      .map(person => (<Person>person))
      .map(person => {
        this.contributorSelector.select(person);
        return true;
      });
    })
    .do(() => this.loading = false)
    .subscribe(() => {
      this.referenceSource.init();
      this.personSource.init();
      this.workSource.init();
      this.subjectSource.init();
    }, error => {
      this.loading = false;
      this.error = error;
    });

  }

  ngOnChanges() {
    this.editForm.reset();
  }

  createForm() {
    this.editForm = this.fb.group({
      title: ['', Validators.required],
      body: ['', Validators.required],
    });
  }

  personCanPublish(): Observable<boolean> {
    return this.currentPersonService.isProjectEditor();
  }

  // Does the initialization and returns empty Observable
  initEdit(params: Params): Observable<any> {
    this.isEdit = true;
    return this.articleApi.createEdits(params['id'], {})
    .map(res => (<Edit>res))
    .map(edit => {
      this.editForm.patchValue(edit);
      this.edit = edit;
      this.edit.references.forEach(refId => {
        this.referenceApi.findById(refId)
        .map(ref => (<Reference>ref))
        .subscribe(ref => {
          this.referenceSelector.select(ref);
        });
      });
      this.edit.contributors.forEach(personId => {
        this.personApi.findById(personId)
        .map(person => (<Person>person))
        .subscribe(person => {
          this.contributorSelector.select(person);
        });
      });
      this.edit.relatedWorks.forEach(workId => {
        this.workApi.findById(workId)
        .map(work => (<Work>work))
        .subscribe(work => {
          this.workSelector.select(work);
        });
      });
      this.edit.subjects.forEach(conceptId => {
        this.conceptApi.findById(conceptId)
        .map(concept => (<Concept>concept))
        .subscribe(concept => {
          this.subjectSelector.select(concept);
        });
      });
      return Observable.empty();
    });
  }

  readEditFromForm() {
    const fv = this.editForm.value;
    this.edit.body = fv.body;
    this.edit.title = fv.title;
    this.edit.references = this.referenceSelector.items.map(ref => ref.id);
    this.edit.contributors = this.contributorSelector.items.map(person => person.id);
    this.edit.subjects = this.subjectSelector.items.map(concept => concept.id);
    this.edit.relatedWorks = this.workSelector.items.map(work => work.id);
  }

  save(): void {
    this.readEditFromForm();

    if (!this.isEdit) {
      this.projectApi.createArticles(PROJECT_ID,
      {
        title: this.edit.title,
        body: this.edit.body,
        referenceId: this.edit.references,
        contributorId: this.edit.contributors,
        conceptId: this.edit.subjects,
        workId: this.edit.relatedWorks,
        status: this.edit.status,
      })
      .subscribe(article => {
        this.editForm.reset();
        this.router.navigate(['articles/', article.id]);
      }, error => this.modalService.open(new ErrorModal(error.statusCode, error.message)));
    } else {
      this.edit.finished = true;
      this.route.params
      .switchMap((params: Params) => this.articleApi.updateByIdEdits(params['id'], this.edit.id, this.edit ))
      .subscribe(edit => {
        this.editForm.reset();
        this.router.navigate(['articles/', edit.articleId]);

      }, error => this.modalService.open(new ErrorModal(error.statusCode, error.message)));
    }
  }

  discard(): void {
    this.modalService.open(new ConfirmModal('Discard changes?', 'You will lose all changes made'))
    .onApprove(() => {
      this.editForm.reset();
      if (this.isEdit) {
        this.route.params
        .switchMap((params: Params) => this.articleApi.destroyByIdEdits(params['id'], this.edit.id))
        .subscribe(() => this.goBack());
      } else {
        this.goBack();
      }
    });
  }

  goBack(): void {
    this.location.back();
  }

  subjectLabel(subject: Concept): string {
    return subject.prefLabel.find(l => l.lang === PREF_LANG).name;
  }
}
