import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { LoopBackAuth } from '../../shared/sdk/services';
import { PROJECT_ID, PREF_LANG } from '../../shared/project.config';
import { Article, PublicComment } from '../../shared/sdk/models';
import { ArticleApi, PublicCommentApi, EditorialCommentApi } from '../../shared/sdk/services';
import { CurrentPersonService } from '../../shared/sign-in/current-person.service';
import { ArticleEditorialCommentInterface, ArticlePublicCommentInterface, ArticleCommentComponent} from '../article-comment/article-comment.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/Rx';

import { SuiModalService } from 'ng2-semantic-ui';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css'],
})
export class ArticleDetailComponent implements OnInit {

  private article: Article = null;
  private canEdit = false;
  private error: Error = null;
  private publicComment = "";
  private publicReply: any = null;
  private publicComments: Array<PublicComment> = [];
  private editorialCommentInterface: ArticleEditorialCommentInterface;
  private publicCommentInterface: ArticlePublicCommentInterface;

  constructor(
    private articleApi: ArticleApi,
    private publicCommentApi: PublicCommentApi,
    private editorialCommentApi: EditorialCommentApi,
    private route: ActivatedRoute,
    private location: Location,
    private auth: LoopBackAuth,
    private modalService: SuiModalService,
    private currentPersonService: CurrentPersonService,
    private titleService: Title,
    ) {
    this.publicCommentInterface = new ArticlePublicCommentInterface(publicCommentApi, articleApi, currentPersonService);
    this.editorialCommentInterface = new ArticleEditorialCommentInterface(editorialCommentApi, articleApi, currentPersonService);
  }

  ngOnInit() {
    this.loadArticle()
    .map(article => {
      this.article = article;
      this.titleService.setTitle(article.title);
      if (this.auth.getCurrentUserId()) {
        this.canEdit = article.contributors.some( contributor => contributor.id === this.auth.getCurrentUserId());
        if (!this.canEdit) {
          this.currentPersonService.isProjectEditor().subscribe(isEditor => this.canEdit = isEditor);
        }
      }
    })
    .flatMap(() => this.publicCommentInterface.init(this.article))
    .flatMap(() => this.editorialCommentInterface.init(this.article))
    .subscribe(() => { },
      error => {
        this.article = null;
        this.error = error; // triggers error-component directive
      }
    );
  }

  private loadArticle(): Observable<Article> {
    return this.route.params
    .switchMap((params: Params) => this.articleApi.findById(params['id'], { include: ['references','contributors', 'publicComments', 'subjects', 'relatedWorks'] }));
  }

  private getSubject(concept) {
    return concept.prefLabel.find(l => l.lang === PREF_LANG).name;
  }
}
