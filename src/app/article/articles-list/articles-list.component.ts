import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { LoopBackConfig } from '../../shared/sdk/index';
import { PROJECT_ID } from '../../shared/project.config';
import { ArticleSource } from '../../shared/search-sources'
import { Article, Project } from '../../shared/sdk/models';
import { ArticleApi, ProjectApi } from '../../shared/sdk/services';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Title } from '@angular/platform-browser';
import 'rxjs/Rx';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})

export class ArticlesListComponent implements OnInit {

  private title = "Articles";

  constructor(
    private articleSource: ArticleSource,
    private titleService: Title) {
  }

  ngOnInit() {
    this.articleSource.init();
    this.titleService.setTitle(this.title);
  }

}
