import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleComponent } from './article.component';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { ArticleEditComponent } from './article-edit/article-edit.component';

import { AuthGuard } from '../shared/auth-guard.service';
import { CanDeactivateGuard } from '../shared/can-deactivate-guard.service';

const articleRoutes: Routes = [
  {
    path: 'articles',
    component: ArticleComponent,
    children: [
      {
        path: '',
        component: ArticlesListComponent
      },
      {
        path: 'create',
        component: ArticleEditComponent,
        canActivate: [AuthGuard],
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: ':id',
        component: ArticleDetailComponent
      },
      {
        path: ':id/edit',
        component: ArticleEditComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
];
@NgModule({
  imports: [ RouterModule.forChild(articleRoutes) ],
  exports: [ RouterModule ]
})
export class ArticleRoutingModule {}
