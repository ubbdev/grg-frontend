import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { MdeditorComponent } from '../shared/mdeditor/mdeditor.component';

import { ArticleComponent } from './article.component';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { ArticleEditComponent } from './article-edit/article-edit.component';
import { ArticleCommentComponent } from './article-comment/article-comment.component';

import { ArticleRoutingModule } from './article-routing.module';
import { BibliographyModule } from '../bibliography/bibliography.module';

@NgModule({
  imports: [
    SharedModule,
    ArticleRoutingModule,
    BibliographyModule
  ],
  declarations: [
    MdeditorComponent,
    ArticleComponent,
    ArticlesListComponent,
    ArticleDetailComponent,
    ArticleEditComponent,
    ArticleCommentComponent
  ],
  exports: [  ],
  providers: [  ]
})
export class ArticleModule {}
