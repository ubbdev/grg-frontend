import { GrgFrontendPage } from './app.po';

describe('grg-frontend App', () => {
  let page: GrgFrontendPage;

  beforeEach(() => {
    page = new GrgFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
